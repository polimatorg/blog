+++
title = 'Google uvodi Gemini u Android Studio'
date = 2024-04-09T04:51:24+01:00
draft = false
description = "Značajan iskorak u AI tehnologiji"
image = "https://storage.googleapis.com/gweb-uniblog-publish-prod/images/Bard_Gemini_SS.width-1300.png"
imageBig = "https://storage.googleapis.com/gweb-uniblog-publish-prod/images/Bard_Gemini_SS.width-1300.png"
categories = ["ai", "gemini", "google"]
authors = ["ajzi"]
avatar = "/images/favicon.webp"
+++

Kompanija "Google" će nastaviti sa uvođenjem Gemini-a na različite proizvode jer je kompanija objavila da se bot Android Studia
nadograđuje sa "Gemini Pro".

Početkom maja 2023. godine, tokom Googlovog I/O razvojnog događaja, kompanija je predstavila Studio Bot koji pokreće
PaLM-2 model. Kompanija uvodi Gemini u Android studiju u više od 180 zemalja za verziju Android Studio Jellyfisha i novijih
verzija Androida.
U februaru Google je takođe unaprijedio osnovni model za Bard chatbota sa PaLM-2 na Gemini Pro.
Baš kao i Studio Bot, novi Gemini bot je u IDE-u (integrisano razvojno okruženje) i 
programeri mogu postavljati pitanja vezana za kodiranje.

Kompanija je rekla da bi programeri trebali primijetiti poboljšani kvalitet odgovora u dovršavanju koda, 
otklanjanju grešaka, pronalaženju relevantnih resursa i pisanju dokumentacije.
Google je rekao da će se iz razloga privatnosti korisnici morati prijaviti i eksplicitno omogućiti Gemini-u da ga koristi. 
Osim toga, odgovori chatbot-a uglavnom zavise od historije razgovora i konteksta koje je pružio programer.
Kompanija je rekla da korisnici mogu lako pristupiti Gemini API starter template-u putem Android Studija
kako bi dodali generativne AI funkcije u svoje aplikacije.

Zaključak

Google kontinuirano ulaže u razvoj AI alata kao što je Gemini u Android Studiju pokazuje njihovu posvećenost poboljšanju iskustva razvoja softvera. Integracija Gemini-a, koja koristi naprednije kapacitete modela PaLM-2, ukazuje na težnju da se programerima omogući efikasnije rješavanje programerskih izazova kroz naprednu podršku u pisanju koda i otklanjanju bugova. Ovo bi moglo dovesti do bržeg razvojnog ciklusa i unapređenja kvaliteta aplikacija, što potencijalno može imati širok uticaj na ekosistem Android aplikacija.
